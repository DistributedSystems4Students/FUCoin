package fucoin.supervisor;

import akka.japi.Creator;
import fucoin.gui.SuperVisorGuiControl;
import fucoin.gui.SuperVisorGuiControlImpl;

import javax.swing.*;
import java.awt.*;

/**
 * Create SuperVisor with a AWT Window.
 * The window displays the information from the supervisor.
 */
public class SuperVisorCreator implements Creator<SuperVisorImpl> {

    @Override
    public SuperVisorImpl create() throws Exception {
        SuperVisorImpl sv = new SuperVisorImpl();
        SuperVisorGuiControl superVisorGuiControl = new SuperVisorGuiControlImpl(sv);
        sv.setGuiControl(superVisorGuiControl);
        return sv;
    }

}
