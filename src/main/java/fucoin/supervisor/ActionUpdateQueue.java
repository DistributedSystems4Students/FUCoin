package fucoin.supervisor;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.actions.transaction.ActionCommitDistributedCommittedTransfer;
import fucoin.actions.transaction.SuperVisorAction;

import java.util.List;

public class ActionUpdateQueue extends SuperVisorAction {

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, SuperVisorImpl superVisor) {

        List<DistributedCommittedTransferRequest> deletes = superVisor.updateList();

        for (DistributedCommittedTransferRequest outdatedRequest : deletes) {
            ActionCommitDistributedCommittedTransfer acdct = new ActionCommitDistributedCommittedTransfer(outdatedRequest);
            for (ActorRef neighbor : superVisor.getKnownNeighbors().values()) {
                neighbor.tell(acdct, self);
            }
        }
        sleep(self, context, 1000);
        self.tell(this, self);
    }

    private void sleep(ActorRef self, UntypedActorContext context, int sleeptime) {
        try {
            context.unwatch(self);
            Thread.sleep(sleeptime);
            context.watch(self);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
