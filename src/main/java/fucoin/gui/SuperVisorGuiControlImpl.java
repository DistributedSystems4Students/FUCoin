package fucoin.gui;

import fucoin.supervisor.AmountTableModel;
import fucoin.supervisor.SuperVisorImpl;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SuperVisorGuiControlImpl implements SuperVisorGuiControl {
    private SuperVisorImpl superVisor;

    private AmountTableModel amountTableModel;

    private SuperVisorThreadGUI threadGUI;

    private boolean logActive = false;

    public SuperVisorGuiControlImpl(SuperVisorImpl sv) {
        superVisor = sv;
        init();
    }

    private void init() {

        amountTableModel = new AmountTableModel();

        threadGUI = new SuperVisorThreadGUI(this);
        threadGUI.init();

    }

    public void guiTerminated() {
        superVisor.exit();
    }

    @Override
    public void onLeave() {
        threadGUI.dispose();
    }

    @Override
    public void updateTable(String address, String name, int amount) {
        this.amountTableModel.updateTable(address, name, amount);
    }

    private void log(LogMessage logMessage) {
        if (logActive) {
            threadGUI.log(logMessage);
        }
    }

    @Override
    public void addLogMsg(String msg) {
        log(new LogMessage(msg));
    }

    @Override
    public void addTransactionLogMessageSuccess(String message) {
        log(new LogMessage(message, LogMessage.Context.TRANSACTION_SUCCESS));
    }

    @Override
    public void addTransactionLogMessageFail(String message) {
        log(new LogMessage(message, LogMessage.Context.TRANSACTION_FAIL));
    }

    public AmountTableModel getAmountTableModel() {
        return amountTableModel;
    }

    public void setAmountTableModel(AmountTableModel amountTableModel) {
        this.amountTableModel = amountTableModel;
    }

    public void activateLogging() {
        this.logActive = true;
    }

    public void disableLogging() {
        this.logActive = false;
    }

    public boolean isLogActive() {
        return logActive;
    }
}
