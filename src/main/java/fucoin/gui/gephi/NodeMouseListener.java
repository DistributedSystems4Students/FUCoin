package fucoin.gui.gephi;

import org.gephi.graph.api.Node;
import org.gephi.preview.api.PreviewMouseEvent;
import org.gephi.preview.api.PreviewProperties;
import org.gephi.project.api.Workspace;

public interface NodeMouseListener {
    public void mouseClicked(Node node, PreviewMouseEvent event, PreviewProperties properties, Workspace workspace);
}
