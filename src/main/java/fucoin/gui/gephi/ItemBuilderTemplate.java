package fucoin.gui.gephi;

import org.gephi.graph.api.Graph;
import org.gephi.preview.api.Item;
import org.gephi.preview.spi.ItemBuilder;
import org.openide.util.lookup.ServiceProvider;


@ServiceProvider(service = ItemBuilder.class)
public class ItemBuilderTemplate implements ItemBuilder {
    @Override
    public Item[] getItems(Graph graph) {
        return new Item[0];
    }

    @Override
    public String getType() {
        return null;
    }
}
