package fucoin.gui.gephi;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class GephiFileSelector {

    /**
     * Display a file selector to select a gephi graph file
     * @return the selected file or the first graph in the resources directory if none selected
     * @throws IOException
     * @throws URISyntaxException
     */
    public File selectTopology() throws IOException, URISyntaxException {
        JFileChooser fileChooser = new JFileChooser(new File(getClass().getResource("/").toURI()));
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        FileNameExtensionFilter fileNameExtensionFilter = new FileNameExtensionFilter("Graph Files", "gexf");
        fileChooser.setFileFilter(fileNameExtensionFilter);
        int result = fileChooser.showOpenDialog(null);

        if (result == JFileChooser.APPROVE_OPTION) {
            return fileChooser.getSelectedFile();
        }

        return getBundledTopologies().get(0);
    }

    /**
     * Get a list of all bundled topology files (i.e. that are stored in the resources)
     * @throws URISyntaxException
     * @throws IOException
     */
    public List<File> getBundledTopologies() throws URISyntaxException, IOException {
        return Files.list(Paths.get(getClass().getResource("/").toURI()))
                .filter(Files::isRegularFile)
                .filter(path -> path.toString().toLowerCase().endsWith(".gexf"))
                .map(Path::toFile).collect(Collectors.toList());
    }
}
