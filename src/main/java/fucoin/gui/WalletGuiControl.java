package fucoin.gui;


public interface WalletGuiControl extends TransactionLogger {
    void setAddress(String address);

    void setAmount(int amount);

    void addKnownAddress(String address);

    /**
     * Tell the GUI, that the wallet is a remote wallet.
     */
    void setRemote();

}
