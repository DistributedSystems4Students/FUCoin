package fucoin.wallet;

import akka.actor.ActorRef;
import akka.japi.Creator;
import fucoin.gui.WalletGuiControlImpl;

public class WalletCreator implements Creator<AbstractWallet> {
    private ActorRef preKnownNeighbour;
    private String walletName;
    private boolean createGUI;

    public WalletCreator(ActorRef preKnownNeighbour, String walletName, boolean createGUI) {
        this.preKnownNeighbour = preKnownNeighbour;
        this.walletName = walletName;
        this.createGUI = createGUI;
    }

    @Override
    public WalletImpl create() throws Exception {

        WalletImpl wallet = new WalletImpl(preKnownNeighbour, walletName);

        if (createGUI) {
            WalletGuiControlImpl gui = new WalletGuiControlImpl(wallet);
            wallet.setGui(gui);
        }

        return wallet;
    }
}
