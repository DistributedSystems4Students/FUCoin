package fucoin.actions.control;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.actions.transaction.SuperVisorAction;
import fucoin.supervisor.SuperVisorImpl;

/**
 * Announce, that there will be some wallets spawned.
 * Used by the configurator to wait, until all new wallets have their initial money.
 * Send to supervisor.
 */
public class ActionAnnounceWalletCreation extends SuperVisorAction {

    public int numOfWallets;
    public ActorRef observer;

    public ActionAnnounceWalletCreation(int numOfWallets, ActorRef observer) {

        this.numOfWallets = numOfWallets;
        this.observer = observer;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self, UntypedActorContext context, SuperVisorImpl abstractNode) {
        System.out.println("Waiting for wallet transactions");
        abstractNode.setPendingBankCommits(abstractNode.getPendingBankCommits() + numOfWallets);
        abstractNode.setBankCommitObserver(sender);
    }
}
