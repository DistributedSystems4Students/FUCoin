package fucoin.actions.control;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.actions.Action;
import fucoin.configurations.AbstractConfiguration;

/**
 * This message tells the configuration, that the wallets have been spawned successfully
 */
public class ActionWalletCreationDone extends Action<AbstractConfiguration> {
    @Override
    protected void onAction(ActorRef sender, ActorRef self, UntypedActorContext context, AbstractConfiguration abstractNode) {

    }
}
