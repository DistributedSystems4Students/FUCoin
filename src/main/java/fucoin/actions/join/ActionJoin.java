package fucoin.actions.join;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.actions.ClientAction;
import fucoin.wallet.AbstractWallet;

/**
 * This action is used by nodes wanting to join the network.
 */
public class ActionJoin extends ClientAction {

    @Override
    protected void onAction(ActorRef sender, ActorRef self, UntypedActorContext context, AbstractWallet node) {

        // send the joined node all known neighbours from node and a reference to the supervisor
        ActionJoinAnswer aja = new ActionJoinAnswer();
        aja.someNeighbors.putAll(node.getKnownNeighbors());
        sender.tell(aja, self);

        if (node.getRemoteSuperVisorActor() == null) {
            node.deferSendOfSuperVisorActor(sender);
        } else {
            sender.tell(new ActionTellSupervisor(node.getRemoteSuperVisorActor()), self);
        }
    }
}
