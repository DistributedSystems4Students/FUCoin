package fucoin.actions.join;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import akka.dispatch.OnSuccess;
import fucoin.actions.ClientAction;
import fucoin.actions.persist.ActionSearchMyWallet;
import fucoin.wallet.AbstractWallet;
import scala.Function1;
import scala.concurrent.Future;
import scala.runtime.BoxedUnit;

import java.util.HashMap;
import java.util.Map.Entry;

/**
 * This action is the response from a wallet which is already in the network
 * to a wallet which wants to join the network.
 * <p>
 * The node in the network sends all its known neighbours which then become
 * neighbours of the new node. This action also contains a reference to the
 * supervisor. If this is the first time the new node learned about the systems
 * supervisor, it proceeds to register at the supervisor.
 */
public class ActionJoinAnswer extends ClientAction {
    public final HashMap<String, ActorRef> someNeighbors = new HashMap<>();

    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {

        //wallet.addLogMsg("Addressed to " + self.path().name() + " from " + sender.path().name() + ": someNeighbors:" + someNeighbors);

        // your neighbours? my neighbours!
        for (Entry<String, ActorRef> neighbor : someNeighbors.entrySet()) {
            wallet.addKnownNeighbor(neighbor.getKey(), neighbor.getValue());
        }
        for (Entry<String, ActorRef> neighbor : someNeighbors.entrySet()) {
            neighbor.getValue().tell(new ActionSearchMyWallet(wallet.getName()), self);
        }
    }

}
