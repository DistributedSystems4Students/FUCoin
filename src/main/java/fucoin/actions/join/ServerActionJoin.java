package fucoin.actions.join;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.actions.transaction.ActionInvokeDistributedCommittedTransfer;
import fucoin.actions.transaction.SuperVisorAction;
import fucoin.supervisor.SuperVisorImpl;

/**
 * Used by nodes to register at the supervisor. In return, the supervisor
 * sends all its neighbours to the node and initiates the transfer of a fixed amount
 * of FUCs to get started.
 */
public class ServerActionJoin extends SuperVisorAction {
    private String name;

    public ServerActionJoin(String name) {
        this.name = name;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, SuperVisorImpl node) {
        ActionJoinAnswer aja = new ActionJoinAnswer(); // TODO: Might need added TellSupervisor
        aja.someNeighbors.putAll(node.getKnownNeighbors());
        sender.tell(aja, self);
        node.addKnownNeighbor(name, sender);
        self.tell(
                new ActionInvokeDistributedCommittedTransfer(self, sender, 100, self),
                sender);
    }
}
