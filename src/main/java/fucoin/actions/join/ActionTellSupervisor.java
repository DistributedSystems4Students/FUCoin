package fucoin.actions.join;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.actions.ClientAction;
import fucoin.wallet.AbstractWallet;

/**
 * Tell the joining node the supervisor
 */
public class ActionTellSupervisor extends ClientAction {

    public final ActorRef supervisor;

    public ActionTellSupervisor(ActorRef supervisor) {
        this.supervisor = supervisor;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self, UntypedActorContext context, AbstractWallet abstractNode) {
        abstractNode.setRemoteSuperVisorActor(supervisor);
    }
}
