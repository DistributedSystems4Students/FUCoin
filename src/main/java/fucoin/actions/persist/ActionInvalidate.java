package fucoin.actions.persist;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.wallet.AbstractWallet;

/**
 * May be used to delete a stored WalletImpl on another participant
 */
public class ActionInvalidate extends Persist {
    public final String name;

    public ActionInvalidate(String name) {
        this.name = name;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        wallet.backedUpNeighbors.remove(name);
    }
}
