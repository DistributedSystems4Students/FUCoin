package fucoin.actions;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.AbstractNode;

import java.io.Serializable;

public abstract class Action<T extends AbstractNode> implements Serializable {
    private ActorRef self;

    public final void doAction(T abstractNode) {
        this.self = abstractNode.getSelf();
        onAction(abstractNode.getSender(), abstractNode.getSelf(),
                abstractNode.getContext(), abstractNode);
    }

    protected abstract void onAction(ActorRef sender, ActorRef self,
                                     UntypedActorContext context, T abstractNode);
}
