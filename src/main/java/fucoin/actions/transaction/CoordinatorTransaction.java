package fucoin.actions.transaction;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.supervisor.SuperVisorImpl;

public abstract class CoordinatorTransaction extends SuperVisorAction {

    @Override
    protected abstract void onAction(ActorRef sender, ActorRef self,
                                     UntypedActorContext context, SuperVisorImpl abstractNode);

}
