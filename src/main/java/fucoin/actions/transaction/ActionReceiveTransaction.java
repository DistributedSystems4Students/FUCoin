package fucoin.actions.transaction;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.wallet.AbstractWallet;

/**
 * Used to send (positive amount) or retrieve money (negative amount)
 */
public class ActionReceiveTransaction extends Transaction {
    final public int amount;

    public ActionReceiveTransaction(int amount) {
        this.amount = amount;
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        wallet.addAmount(wallet.getAmount());
    }
}
