package fucoin.actions.transaction;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import fucoin.actions.ClientAction;
import fucoin.supervisor.DistributedCommittedTransferRequest;
import fucoin.wallet.AbstractWallet;

public class ActionCommitDistributedCommittedTransfer extends ClientAction {

    private ActorRef source;
    private ActorRef target;
    protected int amount;
    private boolean granted;
    private long timestamp;
    private long id;
    private ActorRef observer;


    public ActionCommitDistributedCommittedTransfer(ActorRef source, ActorRef target,
                                                    int amount, boolean granted, long timestamp, long id,
                                                    ActorRef observer) {
        this.source = source;
        this.target = target;
        this.amount = amount;
        this.granted = granted;
        this.timestamp = timestamp;
        this.id = id;
        this.observer = observer;
    }

    public ActionCommitDistributedCommittedTransfer(
            DistributedCommittedTransferRequest outdatedRequest) {
        this.source = outdatedRequest.getSource();
        this.target = outdatedRequest.getTarget();
        this.amount = outdatedRequest.getAmount();
        this.granted = false;
        this.timestamp = outdatedRequest.getTimeout();
        this.id = outdatedRequest.getId();
        this.observer = outdatedRequest.getObserver();
    }

    @Override
    protected void onAction(ActorRef sender, ActorRef self,
                            UntypedActorContext context, AbstractWallet wallet) {
        //wallet.addLogMsg("ActionCommitDistributedCommittedTransfer is granted? " + granted);
        if (granted) {

            if (source.compareTo(self) == 0) {
                wallet.setAmount(wallet.getAmount() - amount);
                wallet.addTransactionLogMessageSuccess("Sent " + amount + " FUC to " + target.path().name());
            } else if (target.compareTo(self) == 0) {
                wallet.setAmount(wallet.getAmount() + amount);
                wallet.addTransactionLogMessageSuccess("Received " + amount + " FUC from " + source.path().name());
                // recipient should notify a possible observer
                if (observer != null) {
                    observer.tell(new ActionNotifyObserver(source, target, amount, granted, timestamp, id), self);
                }
            }

        } else {
            wallet.addLogMsg("abort transaction with id" + id);

            // rollback
            Integer sourceAmount = wallet.amounts.getOrDefault(source, 0);
            Integer targetAmount = wallet.amounts.getOrDefault(target, 0);
            wallet.amounts.put(source, sourceAmount + amount);
            wallet.amounts.put(target, targetAmount - amount);

            if (source.compareTo(self) == 0) {
                wallet.addTransactionLogMessageFail("Failed to send " + amount + " FUC to " + target.path().name() + " (Commit has not been granted)");
                if (observer != null) {
                    observer.tell(new ActionNotifyObserver(source, target, amount, granted, timestamp, id), self);
                }
            }

        }
    }

}
