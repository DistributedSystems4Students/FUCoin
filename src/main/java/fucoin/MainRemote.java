package fucoin;

import akka.actor.ActorRef;
import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import akka.util.Timeout;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import fucoin.setup.NetworkInterfaceReader;
import fucoin.setup.SelectableNetworkInterface;
import fucoin.setup.SetupDialogPanel;
import fucoin.wallet.WalletImpl;
import scala.concurrent.Await;
import scala.concurrent.duration.FiniteDuration;

import javax.swing.*;
import java.io.File;
import java.util.concurrent.TimeUnit;

public class MainRemote {

    public static void main(String[] args) throws InterruptedException {

        String hostname = NetworkInterfaceReader.readDefaultHostname();

        //Load configuration from current directory or from resources directory of jar
        File file = new File("application.conf");
        Config config = ConfigFactory.parseFile(file);
        if (!file.exists()) {
            System.out.println("Load default application.conf");
            config = ConfigFactory.parseResources("application.conf");
        } else {
            System.out.println("Load local application.conf");
        }

        //Init System Actor System
        ActorSystem system = ActorSystem.create("Remote", ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + hostname).withFallback(config));

        //JPanel dialogPanel = createDialogPanel(hostname);
        SetupDialogPanel dialogPanel = new SetupDialogPanel(hostname);

        Timeout timeout = new Timeout(5, TimeUnit.SECONDS);
        ActorRef preknownNeighbour = null;

        String walletName = null;
        String path;

        while (preknownNeighbour == null) {

            int result = JOptionPane.showConfirmDialog(null, dialogPanel, "Connect to wallet network", JOptionPane.OK_CANCEL_OPTION);

            // terminate if user clicked cancel
            if (result == JOptionPane.CANCEL_OPTION) {
                if (system != null) {
                    system.terminate();
                }
                return;
            }

            SelectableNetworkInterface selectedHostname = dialogPanel.getNetworkInterface();

            if (!selectedHostname.getHostName().equals(hostname)) {
                if (system != null) {
                    try {
                        Await.result(system.terminate(), new FiniteDuration(5, TimeUnit.SECONDS));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                hostname = selectedHostname.getHostName();
                system = ActorSystem.create("Remote", ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + hostname).withFallback(config));
            }

            walletName = dialogPanel.getWalletName();
            path = dialogPanel.getAddressOfNeighbour();

            // check input
            if (path.equals("") || walletName.equals("")) {
                continue;
            }

            // resolve the given address
            ActorSelection selection = system.actorSelection(path);

            try {
                preknownNeighbour = Await.result(selection.resolveOne(timeout), timeout.duration());
            } catch (Exception e) {
                System.err.println("Something went wrong while resolving: " + e.getMessage());
            }
        }

        // spawn wallet
        system.actorOf(WalletImpl.props(preknownNeighbour, walletName, true), walletName);

    }
}
