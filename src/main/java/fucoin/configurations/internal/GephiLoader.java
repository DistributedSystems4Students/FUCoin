package fucoin.configurations.internal;

import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphController;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;

public class GephiLoader {

    private Workspace workspace;

    /**
     * Load a graph file that is in the resources directory of the application
     *
     * @throws URISyntaxException
     * @throws FileNotFoundException
     */
    public Graph loadFileFromResources(String path) throws URISyntaxException, FileNotFoundException {
        return loadFile(new File(getClass().getResource(path).toURI()));
    }

    /**
     * Initialize the Gephi toolkit and load the provided file
     *
     * @throws FileNotFoundException
     */
    public Graph loadFile(File file) throws FileNotFoundException {
        ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);

        pc.newProject();
        workspace = pc.getCurrentWorkspace();

        Container container;
        ImportController importController = Lookup.getDefault().lookup(ImportController.class);
        container = importController.importFile(file);


        importController.process(container, new DefaultProcessor(), workspace);

        return Lookup.getDefault().lookup(GraphController.class).getGraphModel(workspace).getGraph();
    }

    public Workspace getWorkspace() {
        return workspace;
    }
}
