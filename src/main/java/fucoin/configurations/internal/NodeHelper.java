package fucoin.configurations.internal;

import org.gephi.graph.api.Node;

public class NodeHelper {

    public static String nameOfNode(Node n) {
        String label = n.getLabel();

        if (label == null || label.isEmpty()) {
            return "Wallet" + n.getId();
        }

        label = label.replace(" ", "_");

        if (Character.isDigit(label.charAt(0))) {
            return "Wallet" + label;
        }

        return label;
    }

}
