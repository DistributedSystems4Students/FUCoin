package fucoin.configurations;

import akka.actor.ActorRef;
import akka.util.Timeout;
import fucoin.actions.control.ActionWalletSendMoney;
import fucoin.configurations.internal.ConfigurationName;
import scala.concurrent.duration.Duration;

import java.util.concurrent.ThreadLocalRandom;

/**
 * This configuration is the previous default of 2 wallets with GUI and a supervisor.
 */
@ConfigurationName("Default Configuration")
public class DefaultConfiguration extends AbstractConfiguration {

    private ThreadLocalRandom rand;
    private Timeout timeout = new Timeout(Duration.create(10, "seconds"));

    public DefaultConfiguration(){
        rand = ThreadLocalRandom.current();
    }

    @Override
    public void run() {
        initSupervisor();

        ActorRef wallet1 = null;
        ActorRef wallet2 = null;
        try {
            wallet1 = spawnWallet("Wallet0", true);
        } catch (Exception e) {
            System.out.println("Wallet0 spawning timed out");
        }
        try {
            wallet2 = spawnWallet("Wallet1", true);
        } catch (Exception e) {
            System.out.println("Wallet1 spawning timed out");
        }

        if (wallet1 != null && wallet2 != null) {
            wallet1.tell(new ActionWalletSendMoney(wallet2.path().name(), 50, getSelf()), wallet1);
        }

    }


    @Override
    public void onReceive(Object message) {
        super.onReceive(message);
    }
}
